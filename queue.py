import heapq

class PriorityQueue(object):
  """
  A priority queue built on top of Python's priority queue implementation in the heapq module to allow for
  priority updates and arbitrary deletions of certain nodes (heapq.heappop only allows for removal of the smallest node)
  """

  REMOVED = '<removed>' # Placeholder for removed task

  def __init__(self, heap=[]):
    heapq.heapify(heap) # Make sure the heap is actually heapified
    self.heap = heap
    self.entry_finder = {} # Dict used to map tasks to nodes in the heap

  def add_with_priority(self, node, priority=0):
    """
    Adds a node with a given priority. Can be used for priority updates if the
    node already exists
    :param node: node to be added
    :param priority: priority of the node
    """
    if node in self.entry_finder:
      self.remove(node)

    entry = [priority, node] # Using a list rather than a tuple because tuples are immutable
    self.entry_finder[node] = entry
    heapq.heappush(self.heap, entry)

  def remove(self, node):
    """
    Removes a given node from the queue. Mark removed entries as REMOVED rather than
    actually deleting them to maintain the heap invariant
    :param node: node to be removed
    :return priority of the removed node
    """
    entry = self.entry_finder.pop(node)
    entry[-1] = self.REMOVED
    return entry[0]

  def extract_min(self):
    """
    Extracts the node with lowest priority
    :return node: node with the lowest priority
    """
    while self.heap:
      priority, node = heapq.heappop(self.heap)

      if node is not self.REMOVED and node in self.entry_finder:
        del self.entry_finder[node]
        return node

    raise KeyError('Extract min failed. Priority queue is empty')

  def size(self):
    """
    Gets size of queue
    :return size of queue
    """
    return len(self.heap)

  def isEmpty(self):
    """
    Checks if queue is empty
    :return true if queue is empty, false otherwise
    """
    return self.size() <= 0
    