import math
from queue import PriorityQueue

def earth_dist(a, b, unit='mi'):
  """
  Calculates shortest distance between two points on the Earth
  using the Haversine formula (https://en.wikipedia.org/wiki/Haversine_formula)

  :param a: latlng pair (tuple), starting point
  :param b: latlng pair (tuple), ending point
  :param unit: 'mi' to calculate in miles, 'km' to calculate in kilometers
  :return the shortest distance between two Earth points or -1 if an invalid unit was passed
  """

  r = 0

  if unit == 'mi':
    r = 3959 # Radius of the Earth in mi
  elif unit == 'km':
    r = 6371 # Radius of the Earth in km
  else:
    return -1 # Need a valid unit of measure

  lat1 = math.radians(a[0])
  lat2 = math.radians(b[0])
  lng1 = math.radians(a[1])
  lng2 = math.radians(b[1])

  comp_1 = math.sin((lat2 - lat1) / 2)**2
  comp_2 = math.cos(lat1) * math.cos(lat2) * math.sin((lng2 - lng1) / 2)**2

  inner = math.sqrt(comp_1 + comp_2)

  return 2 * r * math.asin(inner)

def dijkstra_shortest_dist(graph, start, end):
  """
  Calculates distance of shortest path between two nodes in a graph 
  with an implementation of Dijkstra's shortest path algorithm using a min-priority queue
  :param graph: given graph to find the shortest path
  :param start: starting node
  :param end: end node
  :return the distance of the shortest path or -1 if a path does not exist
  """
  dist = {} # Keeps track of distance for each node
  prev = {} # Keeps track of the previous node in the shortest path for a particular node
  dist[start] = 0

  unvisited = PriorityQueue() # min-priority queue

  for node in graph:
    if node != start:
      dist[node] = float('inf')
      prev[node] = None

    unvisited.add_with_priority(node, dist[node])

  while not unvisited.isEmpty():
    curr = unvisited.extract_min()

    if curr == end:
      return dist[end]

    for neighbor in graph[curr]:
      curr_shortest_dist = dist[curr] + earth_dist(curr, neighbor)

      if curr_shortest_dist < dist[neighbor]:
        dist[neighbor] = curr_shortest_dist
        prev[neighbor] = curr
        unvisited.add_with_priority(neighbor, curr_shortest_dist) # Update priority

  return -1 # End node not found

def find_shortest_detour_dist(graph, stops=[]):
  """
  Find the shortest detour path that goes from point A to point B while also
  going through a certain set of points along the way. Iteratively uses Dijkstra's shortest path algorithm
  to find the shortest path between each of the stopping points to construct the overall shortest path
  between point A and point B for a detour path
  :param graph: given graph to find the shortest detour path
  :param stops: a list of stops with index 0 being the starting point and index len(stops) - 1 being the ending point
  :return the distance of the shortest detour path or -1 if the detour path is not possible
  """
  dist = 0

  for i in range(len(stops) - 1):
    start = stops[i]
    end = stops[i + 1]

    curr_dist = dijkstra_shortest_dist(graph, start, end)

    if curr_dist != -1:
      dist += curr_dist
    else:
      return -1

  return dist
