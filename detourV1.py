from path import earth_dist

def get_shortest_detour_dist(a, b, c, d):
  """
  Calculates the detour distance between two different rides
  :param a: latlng pair (tuple), starting point for driver 1
  :param b: latlng pair (tuple), ending point for driver 1
  :param c: latlng pair (tuple), starting point for driver 2
  :param d: latlng pair (tuple), ending point for driver 2
  :return float value representing the shorter of the detour distances that
  the drivers would need to take to pick-up/drop-off the other driver
  """

  # Detour options:
  # Driver 1 can start at point A, pick up driver 2 at point C,
  # drop driver 2 off at point D, and then drive to point B
  # i.e. A->C->D->B
  # OR
  # Driver 2 can start at point C, pick up driver 1 at point A,
  # drop driver 1 off at point B, and then drive to point D
  # i.e. C->A->B->D
  # 
  # A->C is same dist as C->A so we can just compare
  # C->D->B with A->B->D
  # But D->B is also the same dist as B->D so we can just compare
  # C->D with A->B
  
  detour_1_dist = earth_dist(c, d)
  detour_2_dist = earth_dist(a, b)

  print "Driver 1 detour portion (C->D): " + str(detour_1_dist) + "mi"
  print "Driver 2 detour portion (A->B): " + str(detour_2_dist) + "mi"

  shortest = min(detour_1_dist, detour_2_dist)

  # Add distance(A->C) and distance(D->B) back on
  # to get the total distance of the detour path
  return earth_dist(a, c) + earth_dist(d, b) + shortest
