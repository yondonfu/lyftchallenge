from path import find_shortest_detour_dist
  
def get_shortest_detour_dist(graph, a, b, c, d):
  """
  Calculates the detour distance between two different rides
  :param graph: given graph representing the road/intersection network for the drivers
  :param a: latlng pair (tuple), starting point for driver 1
  :param b: latlng pair (tuple), ending point for driver 1
  :param c: latlng pair (tuple), starting point for driver 2
  :param d: latlng pair (tuple), ending point for driver 2
  :return float value representing the shorter of the detour distances that
  the drivers would need to take to pick-up/drop-off the other driver
  """

  # Detour options:
  # Driver 1 can start at point A, pick up driver 2 at point C,
  # drop driver 2 off at point D, and then drive to point B
  # i.e. A->C->D->B
  # OR
  # Driver 2 can start at point C, pick up driver 1 at point A,
  # drop driver 1 off at point B, and then drive to point D
  # i.e. C->A->B->D
  
  detour_1_dist = find_shortest_detour_dist(graph, [a, c, d, b])
  detour_2_dist = find_shortest_detour_dist(graph, [c, a, b, d])

  print "Driver 1 detour: " + str(detour_1_dist) + "mi"
  print "Driver 2 detour: " + str(detour_2_dist) + "mi" 

  return min(detour_1_dist, detour_2_dist)

