Lyft Programming Challenge

File structure:

detourV1.py contains the first, more basic solution

detourV2.py contains the second, more complex solution using a graph and Dijkstra's algorithm

path.py contains functions for calculating distances and shortest paths between points

queue.py contains an implementation of a priority queue

test.py contains testing functions for both detourV1.py and detourV1.py and prints out their results for the given test cases

Instructions to run:

**Note: These solutions and tests use Python2.7**

To view the test output just type:

python test.py

Overview:

There are 2 solutions provided.

I use the Haversine formula to calculate the distance between two coordinate points because it is a more realistic distance measurement given the Earth's spherical shape than using euclidian distance.

The first solution directly tackles the challenge using the information given. The detour path for driver 1 is as follows: start at point A, pick up driver 2 at point C, drop driver 2 off at point D, and then drive to point B. The detour path for driver 2 is as follows: start at point C, pick up driver 1 at point A, drop off driver 1 at point B, and then drive to point D. Thus, we are comparing the distances of the paths A->C->D->B and C->A->B->D. However, since distance(A->C) = distance(C->A) and distance(D->B) = distance(B->D) we can just compare distance(C->D) with distance(A->B) to determine which driver has the shorter detour path.

The second solution takes into account that this detour scenario is probably taking place in an area with a network of roads and intersections. Consequently, traveling between two coordinate points is not as simple as driving directly from the start point to the end point; the driver must take the roads available to him/her in order to reach a point. Since a network of roads and intersections can be represented by a graph, I calculate the shortest detour path by iteratively using Dijkstra's shortest path algorithm to find the shortest path to each of the stops on a detour path until the end destination is reached. This approach of finding the shortest path to each stop should construct the shortest overall path between two points given a set of points that must be stopped at along the way.



