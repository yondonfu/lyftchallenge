import unittest

import detourV1
import detourV2
import path

"""
Test case for a detourV1
"""
class DetourV1TestCase(unittest.TestCase):
  def setUp(self):
    """
    Set up test coordinates
    """
    self.a = (37.757101, -122.437766)
    self.b = (37.777865, -122.410646)
    self.c = (37.751172, -122.418121)
    self.d = (37.776693, -122.425018)

  def test_detour(self):
    print "===== Detour V1 ======"

    shortest = detourV1.get_shortest_detour_dist(self.a, self.b, self.c, self.d)

    print "Shortest detour: " + str(shortest) + "mi"

    print "===== END ====="

"""
Test case for detourV2
"""
class DetourV2TestCase(unittest.TestCase):
  def setUp(self):
    """
    Set up test coordinates and test graph representing road/intersection network for drivers
    """
    test_graph = {}
    pt_a = (37.757101, -122.437766)
    pt_b = (37.777865, -122.410646)
    pt_c = (37.751172, -122.418121)
    pt_d = (37.776693, -122.425018)
    pt_e = (37.777676, -122.416632)
    pt_f = (37.762250, -122.434654)
    pt_g = (37.761694, -122.424043)

    # print "A-F: " + str(path.earth_dist(pt_a, pt_f))
    # print "A-G: " + str(path.earth_dist(pt_a, pt_g))
    # print "F-G: " + str(path.earth_dist(pt_f, pt_g))
    # print "F-C: " + str(path.earth_dist(pt_f, pt_c))
    # print "F-E: " + str(path.earth_dist(pt_f, pt_e))
    # print "G-E: " + str(path.earth_dist(pt_g, pt_e))
    # print "D-E: " + str(path.earth_dist(pt_d, pt_e))
    # print "D-B: " + str(path.earth_dist(pt_d, pt_b))
    # print "B-C: " + str(path.earth_dist(pt_b, pt_c))
    # print "C-E: " + str(path.earth_dist(pt_c, pt_e))
    # print "B-E: " + str(path.earth_dist(pt_b, pt_e))
    # print "G-D: " + str(path.earth_dist(pt_g, pt_d))

    test_graph[pt_a] = set([pt_f, pt_g])
    test_graph[pt_b] = set([pt_c, pt_e, pt_d])
    test_graph[pt_c] = set([pt_f, pt_e, pt_b])
    test_graph[pt_d] = set([pt_g, pt_e, pt_b])
    test_graph[pt_e] = set([pt_c, pt_b, pt_d, pt_g, pt_f])
    test_graph[pt_f] = set([pt_a, pt_g, pt_e, pt_c])
    test_graph[pt_g] = set([pt_a, pt_f, pt_e, pt_d])

    self.graph = test_graph

    self.a = pt_a
    self.b = pt_b
    self.c = pt_c
    self.d = pt_d

  def test_detour(self):
    print "===== Detour V2 (Graph + Dijkstra's) ====="

    shortest = detourV2.get_shortest_detour_dist(self.graph, self.a, self.b, self.c, self.d)
    
    print "Shortest detour: " + str(shortest) + "mi"

    print "===== END ====="
 

if __name__ == '__main__':
  unittest.main()